﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameControl : MonoBehaviour {
	
	public GameObject clickedFullLetter;
	public List<GameObject> lettersInCorrectOrder;
	//public GameObject fabs;
	public bool topHalfListOff = true;
	public bool bottomHalfListOff = true;
	public bool FullLetterListOff = false;//add the algorithm to turn this true to checkLists()
	public List<string> gameObjectConvertedToLettersList;
	bool gameOverFlag = false;
	

	// Update is called once per frame
	void Update ()
	{
		castRay();
		checkLists();

		//this prevents color of full letters changing while partial letters are still on screen
		if (!topHalfListOff && !bottomHalfListOff)
		{
			foreach(GameObject ob in this.GetComponent<DisplayLetterControls>().FullLetterList)
			{
				ob.layer = 0;
			}
		}

	}
	
	void castRay()
	{ 
		if(Input.GetMouseButtonDown(0))
		{
			
			Ray rayOrigin = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hitInfo;
			Physics.Raycast(rayOrigin, out hitInfo);	
			if(hitInfo.rigidbody != null)
			{
				if(!topHalfListOff && !bottomHalfListOff)
				{
					clickedFullLetter = hitInfo.transform.gameObject;
					if(clickedFullLetter.transform.position.y == 0.0f)
					{
						offAndReappear();
						displayCorrectOrder();
						GameOver();
					}
					else
					{
						LettersInWrongOrder();
					}
					
					
				}
			}
			
		}
	}
	
	//this makes sure that all half-letter objects are off the screen before allowing user
	//to click on the full-letter objects, otherwise the half-letter objects end up running
	//this code after the first full-letter object is instantiated since it's script is then running
	public void checkLists()
	{
		foreach(GameObject ob in  this.GetComponent<DisplayLetterControls>().TopOfLetterList)
		{
			if(ob.activeInHierarchy)
			{
				return;
			}
			else
			{	
				//For some reason the "F" letter activates this early, shutting off the laser sound
				topHalfListOff = false;
			}
			
		}
		
		foreach(GameObject ob in this.GetComponent<DisplayLetterControls>().BottomOfLetterList)
		{
			if(ob.activeInHierarchy)
			{
				return;
			}
			else
			{
				bottomHalfListOff = false;
			}
		}
	}
	
	//makes object invisible on given line but makes it visible on next line down
	void offAndReappear()
	{
		//this ensures that the full letter being clicked is being pulled from the FullLetterList and not
		//from the LettersInCorrectOrder list, since its y-position is always at 0.
		lettersInCorrectOrder.Add(clickedFullLetter);
		clickedFullLetter.SetActive(false);
	}
	
	//displays letters in the order user guesses is correct
	void displayCorrectOrder()
	{
		
		float letterSpacer = -9f;
		foreach(GameObject ob in lettersInCorrectOrder)
		{
			ob.transform.position = new Vector3(letterSpacer, -5.0f, this.GetComponent<DisplayLetterControls>().lettersZPosition);
			//ob.transform.position = new Vector3(letterSpacer, -11.0f, 33.0f);
			ob.SetActive(true);
			letterSpacer += 3.0f;
			ob.GetComponent<AudioSource>().enabled = false;
		}
	}
	
	void LettersInWrongOrder()
	{
		//turns off whole line of letters to force user to start over if they made a mistake
		//may change this in the future but for now it's easier to program the game like this
		foreach(GameObject ob in lettersInCorrectOrder)
		{
			ob.SetActive(false);
		}
		
		//makes the shuffled full letter list reappear on the line above i.e.(position.y=10)
		this.GetComponent<DisplayLetterControls>().OutofOrderFullLetterListDisplay();
		
		lettersInCorrectOrder.Clear();
		gameObjectConvertedToLettersList.Clear();
	}
	
	void GameOver()
	{
		switch(clickedFullLetter.tag)
		{
		case "FullA":
			gameObjectConvertedToLettersList.Add("A");
			break;
			
		case "FullB":
			gameObjectConvertedToLettersList.Add("B");
			break;
		case "FullC":
			gameObjectConvertedToLettersList.Add("C");
			break;
		case "FullD":
			gameObjectConvertedToLettersList.Add("D");
			break;
		case "FullE":
			gameObjectConvertedToLettersList.Add("E");
			break;
		case "FullF":
			gameObjectConvertedToLettersList.Add("F");
			break;
		case "FullG":
			gameObjectConvertedToLettersList.Add("G");
			break;
		case "FullH":
			gameObjectConvertedToLettersList.Add("H");
			break;
		case "FullI":
			gameObjectConvertedToLettersList.Add("I");
			break;
		case "FullJ":
			gameObjectConvertedToLettersList.Add("J");
			break;
		case "FullK":
			gameObjectConvertedToLettersList.Add("K");
			break;
		case "FullL":
			gameObjectConvertedToLettersList.Add("L");
			break;
		case "FullM":
			gameObjectConvertedToLettersList.Add("M");
			break;
		case "FullN":
			gameObjectConvertedToLettersList.Add("N");
			break;
		case "FullO":
			gameObjectConvertedToLettersList.Add("O");
			break;
		case "FullP":
			gameObjectConvertedToLettersList.Add("P");
			break;
		case "FullQ":
			gameObjectConvertedToLettersList.Add("Q");
			break;
		case "FullR":
			gameObjectConvertedToLettersList.Add("R");
			break;
		case "FullS":
			gameObjectConvertedToLettersList.Add("S");
			break;
		case "FullT":
			gameObjectConvertedToLettersList.Add("T");
			break;
		case "FullU":
			gameObjectConvertedToLettersList.Add("U");
			break;
		case "FullV":
			gameObjectConvertedToLettersList.Add("V");
			break;
		case "FullW":
			gameObjectConvertedToLettersList.Add("W");
			break;
		case "FullX":
			gameObjectConvertedToLettersList.Add("X");
			break;
		case "FullY":
			gameObjectConvertedToLettersList.Add("Y");
			break;
		case "FullZ":
			gameObjectConvertedToLettersList.Add("Z");
			break;

		default:
			break;
		}
		
		string finalWord = "";
		foreach(string letter in gameObjectConvertedToLettersList)
		{
			finalWord += letter;
		}
		
		if(this.GetComponent<DisplayLetterControls>().word == finalWord.ToLower())
		{
			gameOverFlag = true;
			this.gameObject.GetComponent<AudioSource>().Stop();

		}
	}

	void OnGUI() {
		GUIStyle myStyle = new GUIStyle (GUI.skin.GetStyle("label"));
		myStyle.alignment = (TextAnchor)TextAlignment.Center;
		myStyle.fontSize = 40;
		if (gameOverFlag) {
			GUI.Label (new Rect (0, 100, Screen.width, Screen.height), "Congratualtions you got the word right.\n\nI bet you feel " +
				"smarter already!!",myStyle);
		}
	}

	
}
