﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


public class DisplayLetterControls : MonoBehaviour {

	public GameObject lastClick;
	public GameObject currentClick;
	public string word;
	public List<GameObject> TopOfLetterList;
	public List<GameObject> BottomOfLetterList;
	public List<GameObject> FullLetterList;
	public GameObject fabs;
	public float lettersZPosition = 0.0f;
	bool correctClick = false;
	public AudioSource laserShot;

	void Start()
	{
		
		//gets a letter from dictionary list, divides it into letters and converts those into game objects and
		//displays objects on screen
		getShowWord();

		
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		//casts a ray to see if mouse was clicked on a "clickable" object
		castRay();			
	}
	
	void getShowWord()
	{
		//This gets a letter and coverts each letter into a game object
		word = GetComponent<ListOfWords>().pickWord();
		
		foreach(char letter in word)
		{
			switch(letter.ToString().ToUpper())
			{
			case "A":
				TopOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabATop));
				BottomOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabABottom));
				break;
			case "B":
				TopOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabBTop));
				BottomOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabBBottom));
				break;
			case "C":
				TopOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabCTop));
				BottomOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabCBottom));
				break;
			case "D":
				TopOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabDTop));
				BottomOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabDBottom));
				break;
			case "E":
				TopOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabETop));
				BottomOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabEBottom));
				break;
			case "F":
				TopOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabFTop));
				BottomOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabFBottom));
				break;
			case "G":
				TopOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabGTop));
				BottomOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabGBottom));
				break;
			case "H":
				TopOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabHTop));
				BottomOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabHBottom));
				break;
			case "I":
				TopOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabITop));
				BottomOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabIBottom));
				break;
			case "J":
				TopOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabJTop));
				BottomOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabJBottom));
				break;
			case "K":
				TopOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabKTop));
				BottomOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabKBottom));
				break;
			case "L":
				TopOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabLTop));
				BottomOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabLBottom));
				break;
			case "M":
				TopOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabMTop));
				BottomOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabMBottom));
				break;
			case "N":
				TopOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabNTop));
				BottomOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabNBottom));
				break;
			case "O":
				TopOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabOTop));
				BottomOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabOBottom));
				break;
			case "P":
				TopOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabPTop));
				BottomOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabPBottom));
				break;
			case "Q":
				TopOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabQTop));
				BottomOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabQBottom));
				break;
			case "R":
				TopOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabRTop));
				BottomOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabRBottom));
				break;
			case "S":
				TopOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabSTop));
				BottomOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabSBottom));
				break;
			case "T":
				TopOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabTTop));
				BottomOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabTBottom));
				break;
			case "U":
				TopOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabUTop));
				BottomOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabUBottom));
				break;
			case "V":
				TopOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabVTop));
				BottomOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabVBottom));
				break;
			case "W":
				TopOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabWTop));
				BottomOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabWBottom));
				break;
			case "X":
				TopOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabXTop));
				BottomOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabXBottom));
				break;
			case "Y":
				TopOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabYTop));
				BottomOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabYBottom));
				break;
			case "Z":
				TopOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabZTop));
				BottomOfLetterList.Add((GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabZBottom));
				break;
			default:
				break;
			}
		}
		
		DisplayDisorderedTopLetterList();
		DisplayDisorderedBottomLetterList();
	}
	
	
	void DisplayDisorderedTopLetterList()
	{
		//use this to seperate each letter when pulling them from the list
		float letterSpacer = -9.0f;
		
		IEnumerable<GameObject> scrambledList = ShuffleOrder(TopOfLetterList);
		//Displays each object from top half letter list
		foreach(GameObject ob in scrambledList)
		{
			
			ob.transform.position = new Vector3(letterSpacer, 6.0f, lettersZPosition);
			letterSpacer += 3.0f;
		}	
	}
	
	void DisplayDisorderedBottomLetterList()
	{
		//use this to seperate each letter when pulling them from the list
		float letterSpacer = -9.0f;
		
		IEnumerable<GameObject> scrambledList = ShuffleOrder(BottomOfLetterList);
		//Displays each object from top half letter list
		foreach(GameObject ob in scrambledList)
		{
			
			ob.transform.position = new Vector3(letterSpacer, 4.0f, lettersZPosition);
			letterSpacer += 3.0f;
		}	
	}	
	
	IEnumerable<GameObject> ShuffleOrder(List<GameObject>ob)
	{
		//This shuffles the order of the letters of the word so they appear in random order on the screen
		//but the variable "word" retains the original letter sequence for later use
		IEnumerable<GameObject> scrambled = ob.OrderBy(x => Random.Range(0,10000));
		return scrambled;
	}
	
	public void OutofOrderFullLetterListDisplay()
	{
		float letterSpacer = -9.0f;
		
		//displays each object from FullLetterList
		foreach(GameObject ob in FullLetterList)
		{
			ob.SetActive(true);
			ob.transform.position = new Vector3(letterSpacer, 0.0f, lettersZPosition);
			letterSpacer += 3.0f;

		}	
	}
	
	void castRay()
	{ 
		if(Input.GetMouseButtonDown(0))
		{
			//makes sure laser shot sound doesn't play once the partial letters are all gone
			if(this.GetComponent<GameControl>().topHalfListOff || this.GetComponent<GameControl>().bottomHalfListOff)
			{
				laserShot.Play();
			}
			Ray rayOrigin = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hitInfo;
			Physics.Raycast(rayOrigin, out hitInfo);	
			if(hitInfo.rigidbody != null)
			{
				//be able to turn off this part
				if((this.GetComponent<GameControl>().topHalfListOff || this.GetComponent<GameControl>().bottomHalfListOff))
				{

					if(currentClick != null)
					{
						lastClick = currentClick;
						lastClick.GetComponent<MeshRenderer>().material.color = Color.yellow;
					}
					currentClick = hitInfo.transform.gameObject;
					currentClick.GetComponent<MeshRenderer>().material.color = Color.yellow;
					compareLetters();
				}
			}
			
		}
		
		
	}
	
	
	//determines which letter-half pairs to turn off and which full letter to create
	void compareLetters()
	{
		if(lastClick != null)
		{
			GameObject tempObject;
			
			switch(lastClick.tag)
			{
			case "TopA": 
				if(currentClick.tag == "BottomA")
				{
					//creates a full letter from these pairs
					//is there a way I can encapsulate this into a method so I don't have to copy and paste this so much????
					//Can't think of one right now
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabA);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			case "BottomA":
				if(currentClick.tag == "TopA")
				{
					//creates a full letter from these pairs
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabA);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
				
			case "TopB": 
				if(currentClick.tag == "BottomB")
				{
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabB);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			case "BottomB":
				if(currentClick.tag == "TopB")
				{
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabB);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			case "TopC": 
				if(currentClick.tag == "BottomC")
				{
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabC);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			case "BottomC":
				if(currentClick.tag == "TopC")
				{
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabC);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			case "TopD": 
				if(currentClick.tag == "BottomD")
				{
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabD);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			case "BottomD":
				if(currentClick.tag == "TopD")
				{
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabD);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			case "TopE": 
				if(currentClick.tag == "BottomE")
				{
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabE);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			case "BottomE":
				if(currentClick.tag == "TopE")
				{
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabE);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			case "TopF": 
				if(currentClick.tag == "BottomF")
				{
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabF);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			case "BottomF":
				if(currentClick.tag == "TopF")
				{
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabF);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			case "TopG": 
				if(currentClick.tag == "BottomG")
				{
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabG);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			case "BottomG":
				if(currentClick.tag == "TopG")
				{
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabG);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			case "TopH": 
				if(currentClick.tag == "BottomH")
				{
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabH);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			case "BottomH":
				if(currentClick.tag == "TopH")
				{
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabH);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			case "TopI": 
				if(currentClick.tag == "BottomI")
				{
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabI);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			case "BottomI":
				if(currentClick.tag == "TopI")
				{
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabI);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			case "TopJ": 
				if(currentClick.tag == "BottomJ")
				{
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabJ);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			case "BottomJ":
				if(currentClick.tag == "TopJ")
				{
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabJ);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			case "TopK": 
				if(currentClick.tag == "BottomK")
				{
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabK);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			case "BottomK":
				if(currentClick.tag == "TopK")
				{
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabK);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			case "TopL": 
				if(currentClick.tag == "BottomL")
				{
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabL);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			case "BottomL":
				if(currentClick.tag == "TopL")
				{
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabL);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			case "TopM": 
				if(currentClick.tag == "BottomM")
				{
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabM);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			case "BottomM":
				if(currentClick.tag == "TopM")
				{
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabM);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			case "TopN": 
				if(currentClick.tag == "BottomN")
				{
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabN);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			case "BottomN":
				if(currentClick.tag == "TopN")
				{
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabN);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			case "TopO": 
				if(currentClick.tag == "BottomO")
				{
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabO);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			case "BottomO":
				if(currentClick.tag == "TopO")
				{
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabO);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			case "TopP": 
				if(currentClick.tag == "BottomP")
				{
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabP);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			case "BottomP":
				if(currentClick.tag == "TopP")
				{
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabP);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			case "TopQ": 
				if(currentClick.tag == "BottomQ")
				{
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabQ);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			case "BottomQ":
				if(currentClick.tag == "TopQ")
				{
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabQ);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			case "TopR": 
				if(currentClick.tag == "BottomR")
				{
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabR);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			case "BottomR":
				if(currentClick.tag == "TopR")
				{
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabR);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			case "TopS": 
				if(currentClick.tag == "BottomS")
				{
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabS);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			case "BottomS":
				if(currentClick.tag == "TopS")
				{
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabS);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			case "TopT": 
				if(currentClick.tag == "BottomT")
				{
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabT);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			case "BottomT":
				if(currentClick.tag == "TopT")
				{
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabT);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			case "TopU": 
				if(currentClick.tag == "BottomU")
				{
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabU);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			case "BottomU":
				if(currentClick.tag == "TopU")
				{
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabU);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			case "TopV": 
				if(currentClick.tag == "BottomV")
				{
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabV);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			case "BottomV":
				if(currentClick.tag == "TopV")
				{
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabV);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			case "TopW": 
				if(currentClick.tag == "BottomW")
				{
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabW);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			case "BottomW":
				if(currentClick.tag == "TopW")
				{
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabW);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			case "TopX": 
				if(currentClick.tag == "BottomX")
				{
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabX);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			case "BottomX":
				if(currentClick.tag == "TopY")
				{
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabX);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			case "TopY": 
				if(currentClick.tag == "BottomY")
				{
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabY);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			case "BottomY":
				if(currentClick.tag == "TopY")
				{
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabY);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			case "TopZ": 
				if(currentClick.tag == "BottomZ")
				{
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabZ);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			case "BottomZ":
				if(currentClick.tag == "TopZ")
				{
					tempObject = (GameObject)Instantiate(fabs.GetComponent<Prefabs>().PrefabZ);
					tempObject.transform.GetComponent<AudioSource>().Play();
					FullLetterList.Add (tempObject);
					OutofOrderFullLetterListDisplay();
					offHalfOnWhole();
				}
				break;
			
			default:
				Debug.Log("What are you doing here?");				
				break;
			}
			lastClick.GetComponent<MeshRenderer>().material.color = Color.green;
			currentClick.GetComponent<MeshRenderer>().material.color = Color.green;
			lastClick = null;
			currentClick = null;
		}
	}
	
	//turns off the half letter pairs 
	void offHalfOnWhole()
	{
		//turns off eah letter half
		lastClick.SetActive(false);
		currentClick.SetActive(false);
		
	}
	//Going to need to import english dictionary, txt file can be found here 
	//http://www.math.sjsu.edu/~foster/dictionary.txt
	
	
	

}
