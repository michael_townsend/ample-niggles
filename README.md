# README #

This is an educational game that I developed using Unity3D and Blender.  It is currently only playable as a browser plug-in.  Many browsers including Chrome and IE do not support the API Unity3D uses, however it does work with Firefox.  Ideally, I'll be able to develop this for the iPad.  

The original code can be found under the "Source" tab.  It is written in C#.  These are not needed to play the game but there in case you want to see how I pulled it off.  I wrote this game as a first semester CS student and now in retrospect I would like to go back and clean it up, time permitting.  There are several other improvements I would like to make as well but again, I just need time!  

To run the game, you will need to download all 4 downloads and make sure they are all in the same folder.  Click on the Builds.html file to start the game.

The sound is from the Unity Asset store but I created everything else in the game.

I would like to thank Thane Plummer who provided a lot of help with the Blender portion and his insight as to automating the creation of the 3D models used in the game using Python.  For any questions feel free to contact me, Michael Townsend, at michael.h.townsend@gmail.com.

I hope you enjoy!